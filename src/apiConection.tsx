export default class Connection {
  static instance: any = null;
  constructor() {
    if (!Connection.instance) {
      Connection.instance = {
        async getData(url: string, headers: {}) {
          try {
            const res = await fetch(url, {
              method: 'GET',
              headers: headers ? headers : {},
            });
            return { resBody: await res.json(), error: res.status !== 200 };
          } catch (e) {
            return { error: true };
          }
        },
        async postData(url: string, body: {}, headers: {}) {
          try {
            const res = await fetch(url, {
              method: 'POST',
              headers: {
                'Content-Type': 'application/json',
                ...headers,
              },
              body: JSON.stringify(body),
            });
            return {
              resBody: await res.json(),
              error: res.status < 200 || res.status > 201,
            };
          } catch (error) {
            return { error: true };
          }
        },
        async patchData(url: string, body: {}) {
          try {
            const res = await fetch(url, {
              method: 'PATCH',
              headers: {
                'Content-Type': 'application/json',
              },
              body: JSON.stringify(body),
            });
            return { error: res.status !== 200, resBody: await res.json() };
          } catch (error) {
            return { error: true };
          }
        },
        async deleteData(url: string) {
          try {
            const res = await fetch(url, {
              method: 'DELETE',
            });
            return { error: res.status !== 200, resBody: await res.json() };
          } catch (error) {
            return { error: false };
          }
        },
      };
    }
    return Connection.instance;
  }
}
