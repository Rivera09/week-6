export const getRealPrice = (price: number, discount: number | null) =>
  discount
    ? price - (price * discount) / 100 + '$'
    : price
    ? price + '$'
    : 'FREE';
