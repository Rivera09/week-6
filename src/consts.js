export const HOME_PAGE = 'HOME';
export const GAMES_LIST = 'GAMES LIST';
export const GAME_DETAILS = 'GAME DETAILS';
export const LOGIN_PAGE = 'LOGIN PAGE';

const BASE_URL = 'https://trainee-gamerbox.herokuapp.com/';

export const LIMIT = 5;

export const GAMES_URL = BASE_URL + `games?_start=:start&_limit=${LIMIT}`;
export const GAMES_NAME_URL =
  BASE_URL + `games?name_contains=:filterName&_start=:start&_limit=${LIMIT}`;
export const FEATURED_GAMES = BASE_URL + 'games?_start=0&_limit=7';
export const TEST_URL = BASE_URL + 'games?_limit=:limit';
export const LOGIN_API_URL = BASE_URL + 'auth/local';
export const POST_COMMENT_URL = BASE_URL + 'games/:gameId/comment';
export const GAME_DATA_URL = BASE_URL + 'games/:gameId';
export const LOGGED_USER_DATA = BASE_URL + 'users/me';

export const LOG_IN_USER_TYPE = 'set local storage';
export const LOG_OUT_USER_TYPE = 'remove local storage';
export const SET_USER_TOKEN_TYPE = 'set user token';
