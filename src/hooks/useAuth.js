import { useState } from 'react';

export default function useAuth() {
  const [userData, setUserData] = useState({});

  return [
    { isAuthed: false, ...userData },
    function (newUserData) {
      setUserData(
        newUserData ? { isAuthed: true, ...newUserData } : { isAuthed: false }
      );
    },
  ];
}
