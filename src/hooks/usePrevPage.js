import { useRef } from 'react';

function usePrevPage() {
  const prevPageRef = useRef();

  return [
    prevPageRef.current,
    function (prevPage) {
      if (prevPage && prevPage !== prevPageRef.current)
        prevPageRef.current = prevPage;
    },
  ];
}

export default usePrevPage;
