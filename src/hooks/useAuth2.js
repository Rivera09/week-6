import { useReducer } from 'react';

import {
  LOG_IN_USER_TYPE,
  LOG_OUT_USER_TYPE,
  LOGGED_USER_DATA,
  SET_USER_TOKEN_TYPE,
} from '../consts';
import Connection from '../apiConection';

const apiConnection = new Connection();

const reducer = (state, action) => {
  const { type, payload } = action;
  switch (type) {
    case LOG_IN_USER_TYPE:
      const { userData } = payload;
      localStorage.setItem('userToken', userData.jwt);
      return { isAuthed: true, ...userData };
    case LOG_OUT_USER_TYPE:
      localStorage.removeItem('userToken');
      return { isAuthed: false };
    case SET_USER_TOKEN_TYPE:
      const { token, user } = payload;
      return { isAuthed: true, jwt: token, user };
    default:
      return state;
  }
};

export default function useAuth() {
  const [userData, dispatch] = useReducer(reducer, { isAuthed: false });

  const getSavedToken = () => {
    const token = localStorage.getItem('userToken');
    if (!token || userData.isAuthed) return;
    apiConnection
      .getData(LOGGED_USER_DATA, {
        Authorization: `Bearer ${token}`,
      })
      .then(({ resBody, error }) => {
        if (error) return;
        dispatch({
          type: SET_USER_TOKEN_TYPE,
          payload: { token, user: resBody },
        });
      });
  };

  getSavedToken();

  return {
    userData,
    logIn: function (userData) {
      dispatch({ type: LOG_IN_USER_TYPE, payload: { userData } });
    },
    logOut: function () {
      dispatch({ type: LOG_OUT_USER_TYPE });
    },
  };
}
