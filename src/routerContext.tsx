import React, { createContext } from 'react';

export const RouterContext = createContext(null);

export const RouterProvider: React.FC<{ value: any }> = ({
  children,
  value,
}) => {
  return (
    <RouterContext.Provider value={value}>{children}</RouterContext.Provider>
  );
};
