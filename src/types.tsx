export interface BackgroundAnimationProps {
  onClick?: Function;
}

export interface CommentProps {
  userName: string;
  comment: string;
}

export interface FilterFormProps {
  handleSubmit: Function;
  handleOnChange: Function;
  value: string;
}

export interface GameCardProps {
  gameData: { cover_art: any; price: number | string; name: string };
}

export interface GameListItemProps {
  gameDetails: {
    name: string;
    price: string | number;
    genre: any;
    publishers: any[];
    cover_art: any;
  };
}

export interface HeaderProps {
  title?: string;
}

export interface NavbarProps {
  logOut: Function;
  userData: any;
}

export interface PrivateRouteProps {
  component: React.FC;
  path: string;
  exact: boolean;
}

export interface GameCommentInterface {
  body: string;
  id: number;
  trainee: string;
  user: string | number;
}

export interface GameDetailsInterface {
  cover_art?: any;
  genre?: any;
  publishers?: any[];
  price?: number | string;
  name?: string;
}
