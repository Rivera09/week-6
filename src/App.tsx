import React, { Suspense } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import { RouterProvider } from './routerContext';
import { useAu } from './hooks';
import { Footer, Navbar, Spinner, PrivateRoute } from './components';
import Connection from './apiConection';

import './sass/style.scss';

const Home = React.lazy(() => import('./pages/Home'));
const GameDetails = React.lazy(() => import('./pages/GameDetails'));
const GamesList = React.lazy(() => import('./pages/GamesList'));
const Login = React.lazy(() => import('./pages/Login'));
const Profile = React.lazy(() => import('./pages/Profile'));

const apiConnection = new Connection();

function App() {
  const { userData, logIn, logOut } = useAu();
  enum Pages {
    HOME = '/',
    GAMES = '/games',
    LOGIN = '/login',
    GAME_DETAILS = '/games/:id',
    PROFILE = '/profile',
  }

  return (
    <Router>
      <RouterProvider
        value={{
          apiConnection,
          userData,
          logIn,
        }}
      >
        <Navbar logOut={logOut} userData={userData} />
        <Suspense fallback={<Spinner />}>
          <Switch>
            <Route path={Pages.HOME} exact component={Home} />
            <Route path={Pages.GAME_DETAILS} exact component={GameDetails} />
            <Route path={Pages.GAMES} component={GamesList} />
            <Route path={Pages.LOGIN} exact component={Login} />
            <PrivateRoute path={Pages.PROFILE} exact component={Profile} />
          </Switch>
        </Suspense>
      </RouterProvider>
      <Footer />
    </Router>
  );
}

export default App;
