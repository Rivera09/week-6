import React, { memo } from 'react';

const Footer: React.FC = () => {
  return (
    <footer>
      <h4>
        &copy; {new Date().getFullYear()} Retro game store. All rights reserved
      </h4>
    </footer>
  );
};

export default memo(Footer);
