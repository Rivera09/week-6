import React, { memo } from 'react';

import { HeaderProps } from '../types';

const Header: React.FC<HeaderProps> = ({ title }) => {
  return (
    <header className="main-header">
      <h1>{title}</h1>
    </header>
  );
};

Header.defaultProps = {
  title: 'Retro game store',
};

export default memo(Header);
