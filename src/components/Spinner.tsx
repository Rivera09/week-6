import React from 'react';
import Spinner from 'react-spinkit';

const SpinnerContainer: React.FC = () => (
  <div className="spinner-container">
    <Spinner name="pacman" color="coral" />
  </div>
);

export default SpinnerContainer;
