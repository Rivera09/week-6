import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

import { NavbarProps } from '../types';
import logo from '../img/logo.png';

const Navbar: React.FC<NavbarProps> = ({ logOut, userData }) => {
  const [active, setActive] = useState<boolean>(false);
  const [dark, setDark] = useState<boolean>(false);

  useEffect(() => {
    window.onscroll = () => {
      const isTop = window.scrollY === 0;
      if (isTop) setDark(() => false);
      else if (!isTop && !dark) setDark(() => true);
    };
  }, [dark]);

  return (
    <nav className={`main-nav ${active ? 'active' : ''} ${dark ? 'dark' : ''}`}>
      <Link to="/">
        <img src={logo} alt="logo" />
      </Link>
      <ul className={active ? 'active' : ''}>
        <li>
          <Link to="/">Home</Link>
        </li>
        <li>
          <Link to="/games">Games</Link>
        </li>
        {userData.isAuthed ? (
          <>
            <li
              onClick={() => {
                if (window.confirm('are you sure you want to log out?')) logOut();
              }}
            >
              Logout
            </li>
            <li>
              <Link to="/profile">Profile</Link>
            </li>
          </>
        ) : (
          <>
            <li>
              <Link to="/login">Login</Link>
            </li>
          </>
        )}
      </ul>
      <div
        className={`burger ${active ? 'active' : ''}`}
        onClick={() => setActive((prevValue) => !prevValue)}
      >
        <div className="line1"></div>
        <div className="line2"></div>
        <div className="line3"></div>
      </div>
    </nav>
  );
};

export default Navbar;
