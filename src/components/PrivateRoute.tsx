import React, { useContext } from 'react';
import { Redirect, Route, useLocation } from 'react-router-dom';
import { RouterContext } from '../routerContext';

import { PrivateRouteProps } from '../types';

const PrivateRoute: React.FC<PrivateRouteProps> = ({
  component: Component,
  ...rest
}) => {
  const location = useLocation();
  const { userData } = useContext<any>(RouterContext);

  return (
    <Route {...rest}>
      {userData.isAuthed ? (
        <Component />
      ) : (
        <Redirect to={{ pathname: '/login', state: { from: location } }} />
      )}
    </Route>
  );
};

export default PrivateRoute;
