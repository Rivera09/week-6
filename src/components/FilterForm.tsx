import React from 'react';

import {FilterFormProps} from '../types'


const filterForm: React.FC<FilterFormProps> = ({
  handleSubmit,
  handleOnChange,
  value,
}) => (
  <form className="filter-games-form" onSubmit={(e) => handleSubmit(e)}>
    <input
      type="text"
      placeholder="Game name"
      name="gameName"
      onChange={(e) => handleOnChange(e)}
      autoComplete="off"
      value={value}
    />
    <button type="submit">Search</button>
  </form>
);

export default filterForm;
