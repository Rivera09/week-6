import React, { memo } from 'react';
import { CommentProps } from '../types';

const Comment: React.FC<CommentProps> = ({ userName, comment }) => (
  <div className="comment">
    <h3>{userName}</h3>
    <p>{comment}</p>
  </div>
);

export default memo(Comment);
