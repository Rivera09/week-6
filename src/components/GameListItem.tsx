import React from 'react';

import { GameListItemProps } from '../types';
import { BackgroundAnimation } from '../components';
import defaultLogo from '../img/logo.png';

const GameListItem: React.FC<GameListItemProps> = ({
  gameDetails: { name, price, genre, publishers, cover_art },
}) => (
  <div className="games-list-item">
    <BackgroundAnimation>
      <div className="list-item-info">
        <div>
          <img src={cover_art?.url || defaultLogo} alt="game logo" />
          <div className="list-item-info-name">
            <h2>
              {name} - {publishers[0]?.name}
            </h2>
            <p>{genre.name}</p>
          </div>
        </div>
        <div className="list-item-info-price">
          <p className="prev-price"></p>
          <p className="price">{price}$</p>
        </div>
      </div>
    </BackgroundAnimation>
  </div>
);

export default GameListItem;
