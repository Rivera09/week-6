import React from 'react';
import { BackgroundAnimationProps } from '../types';

const BackgroundAnimation: React.FC<BackgroundAnimationProps> = ({
  children,
  onClick,
}) => {
  return (
    <div className="animated-bg" onClick={() => (onClick ? onClick() : null)}>
      <div className="list-item-bg">
        <div className="tint-color"></div>
        <div className="secondary-color"></div>
      </div>
      {children}
    </div>
  );
};

export default BackgroundAnimation;
