import React from 'react';

import { GameCardProps } from '../types';
import defaultImage from '../img/hero.jpg';

const GameCard: React.FC<GameCardProps> = ({
  gameData: { cover_art, price, name },
}) => (
  <div className="game-card">
    <div className="game-info">
      <h2 className="game-title">{name}</h2>
      <p className="game-desc"></p>
    </div>
    <div className="game-price">
      <h3>PRICE: </h3>
      <h3>{price}$</h3>
    </div>
    <img src={cover_art?.url || defaultImage} alt={name} className="bg-image" />
  </div>
);

export default GameCard;
