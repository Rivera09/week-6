import React, { useState, useContext } from 'react';

import { Header, BackgroundAnimation, Spinner } from '../components';
import { RouterContext } from '../routerContext';
import { LOGIN_API_URL } from '../consts';
import { Redirect } from 'react-router-dom';

const Login: React.FC = () => {
  const [formData, setFormData] = useState<{
    identifier: string;
    password: string;
  }>({
    identifier: '',
    password: '',
  });
  const [loading, setLoading] = useState<boolean>(false);
  const { apiConnection, logIn, userData } = useContext<any>(RouterContext);

  const handleFormData = (e: any) => {
    setFormData((prevData) => ({
      ...prevData,
      [e.target.name]: e.target.value,
    }));
  };

  const onSubmit = async () => {
    if (!formData.identifier || !formData.password) return;
    try {
      setLoading(true);
      const { resBody, error } = await apiConnection.postData(
        LOGIN_API_URL,
        formData
      );
      if (error) return console.log('error');
      logIn(resBody);
      setLoading(false);
    } catch (error) {}
    // console.log(resBody);
    // setAuthData(resBody);
  };

  // const onCancel = () => {};
  const { identifier, password } = formData;

  if (userData.isAuthed) return <Redirect to="/profile" />;
  if (loading) return <Spinner />;

  return (
    <>
      <Header title="Login" />
      <div className="login-container">
        <form className="login-form">
          <div className="form-input">
            <label>User</label>
            <input
              type="text"
              name="identifier"
              autoComplete="off"
              value={identifier}
              onChange={handleFormData}
            />
          </div>
          <div className="form-input">
            <label>Password</label>
            <input
              type="password"
              name="password"
              value={password}
              onChange={handleFormData}
            />
          </div>
          <div className="login-btns-container">
            {/* <BackgroundAnimation onClick={onCancel}>
              <button type="submit">Cancel</button>
            </BackgroundAnimation> */}
            <BackgroundAnimation onClick={onSubmit}>
              <button type="submit">Log in</button>
            </BackgroundAnimation>
          </div>
        </form>
      </div>
    </>
  );
};

export default Login;
