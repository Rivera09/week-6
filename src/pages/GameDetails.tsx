import React, { useEffect, useState, useContext, useRef } from 'react';
import { useParams } from 'react-router-dom';

import { GameCommentInterface, GameDetailsInterface } from '../types';
import { Comment } from '../components';
import BackgroundAnimation from '../components/BackgroundAnimation';
import { RouterContext } from '../routerContext';
import { POST_COMMENT_URL, GAME_DATA_URL } from '../consts';

import heroImage from '../img/hero.jpg';

const GameDetails: React.FC = () => {
  const { id } = useParams<{ id: string }>();

  const [gameComments, setGameComments] = useState<GameCommentInterface[]>([]);
  const { apiConnection, userData } = useContext<any>(RouterContext);
  const [gameData, setGameData] = useState<GameDetailsInterface>({});
  const inputRef = useRef<HTMLInputElement>(null);

  const updateComments = (newComment: any) => {
    if (newComment instanceof Array)
      return setGameComments((prevComments) => [
        ...prevComments,
        ...newComment,
      ]);
    setGameComments((prevComments) => [...prevComments, newComment]);
  };

  useEffect(() => {
    const fetchGameData = async () => {
      const { resBody, error } = await apiConnection.getData(
        GAME_DATA_URL.replace(':gameId', id)
      );
      if (error) return console.log('error');
      setGameData(resBody);
      setGameComments(resBody.comments);
    };
    fetchGameData();
    // updateComments(comments);
  }, [apiConnection, id]);

  const onSubmit = async (e: any) => {
    e.preventDefault();
    const body = inputRef?.current?.value;
    const headers = {
      Authorization: `Bearer ${userData.jwt}`,
    };
    if (!body) return;
    const { resBody, error } = await apiConnection.postData(
      POST_COMMENT_URL.replace(':gameId', id),
      {
        body,
      },
      headers
    );
    if (error) return console.log('error');
    updateComments({
      body: resBody.body,
      id: resBody.id,
      trainee: resBody.trainee,
      user: resBody.user,
    });
    // inputRef?.current?.value = '';
  };
  // return <h1 style={{ margin: 500 }}>game details</h1>;
  const { cover_art, genre, publishers, price, name } = gameData;

  return (
    <div className="game-details-container">
      <div className="hero">
        <img src={cover_art?.url || heroImage} alt="gta v" />
      </div>
      <div className="game-details">
        <div>
          <h2>
            {name} - {publishers ? publishers[0]?.name : 'Unknown'}
          </h2>
          <p>{genre?.name}</p>
        </div>
        <div className="prices-container">
          <h4>PRICE:</h4>
          <div>
            <p className="prev-price"></p>
            <p className="price">{price}$</p>
          </div>
        </div>
        <p className="game-details-desc">{''}</p>
        <div className="get-btn-container">
          <BackgroundAnimation>
            <button>GET</button>
          </BackgroundAnimation>
        </div>
      </div>
      <div className="comment-section">
        <h2>Comments</h2>
        {gameComments.map(({ id, trainee, body }) => (
          <Comment userName={trainee || 'Anonimous'} comment={body} key={id} />
        ))}
        {userData.isAuthed ? (
          <>
            <h3>Post a comment</h3>
            <form onSubmit={onSubmit}>
              <input ref={inputRef} type="text" />
              <button type="submit">Comment</button>
            </form>
          </>
        ) : (
          <h3>Login to post comments about this game</h3>
        )}
      </div>
    </div>
  );
};

export default GameDetails;
