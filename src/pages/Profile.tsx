import React, { useContext } from 'react';
import { RouterContext } from '../routerContext';
import { Redirect } from 'react-router-dom';
import { Header } from '../components';

const Profile: React.FC = () => {
  const { userData } = useContext<any>(RouterContext);

  if (!userData.isAuthed) return <Redirect to="/login" />;

  const { firstName, lastName, created_at, email, username } = userData?.user;
  return (
    <>
      <Header title="Profile" />
      <div className="user-info">
        <h4>
          Name: {firstName} {lastName}
        </h4>
        <h4>username: {username}</h4>
        <h4>email: {email}</h4>
        <h4>Registered on: {created_at}</h4>
      </div>
    </>
  );
};

export default Profile;
