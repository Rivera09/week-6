import React, { useState, useEffect, useContext, useCallback } from 'react';
import { Link, Redirect, useLocation } from 'react-router-dom';

import { Header, GameListItem, Spinner, FilterForm } from '../components';
import { RouterContext } from '../routerContext';
import { /* GAMES_URL, */ LIMIT, GAMES_NAME_URL, TEST_URL } from '../consts';

const GamesList: React.FC = () => {
  const { apiConnection } = useContext<any>(RouterContext);
  const [gamesList, setGamesList] = useState<any[]>([]);
  // const [currentPagination, setCurrentPagination] = useState(0);
  const [showLoadMoreBtn, setShowLoadMoreBtn] = useState<boolean>(true);
  const [loading, setLoading] = useState<boolean>(true);
  const [filterName, setFilterName] = useState<string>('');
  const [redirectUrl, setRedirectUrl] = useState<string>('');
  const location = useLocation();

  const fetchGames = useCallback(async () => {
    let fromSamePage = false;
    if (redirectUrl) {
      setRedirectUrl('');
      // setCurrentPagination(0);
      fromSamePage = true;
    }
    const queryParams = new URLSearchParams(location.search);
    let name: string | null = queryParams.get('name');
    let page: any = queryParams.get('page');
    if (name) {
      name = name.replaceAll('"', '');
      setFilterName(name);
    }
    if (!page) page = 1;
    const { resBody, error } = await apiConnection.getData(
      !name
        ? TEST_URL.replace(':limit', (page * LIMIT) as any)
        : GAMES_NAME_URL.replace(':start', '0').replace(':filterName', name)
    );
    if (error) return console.log('Error');
    if (fromSamePage) setGamesList(resBody);
    else setGamesList((prevGames) => [...prevGames, ...resBody]);
    if (!resBody.length || resBody.length < LIMIT * +page)
      setShowLoadMoreBtn(false);
    else setShowLoadMoreBtn(true);
    setLoading(false);
  }, [apiConnection, location.search, setRedirectUrl]);

  useEffect(() => {
    fetchGames();
  }, [fetchGames]);

  useEffect(() => {
    const queryParams = new URLSearchParams(location.search);
    let page: any = queryParams.get('page');
    if (+page > 1) {
      window.scrollTo(0, 1000000000);
    }
  }, [gamesList, location.search]);

  const handleOnSubmit = (e: Event) => {
    e.preventDefault();
    if (filterName) setRedirectUrl(`/games?name=${filterName}`);
    else setRedirectUrl('/games');
  };

  const changePage = () => {
    const queryParams = new URLSearchParams(location.search);
    const name: string | null = queryParams.get('name');
    let page: string | number | null = queryParams.get('page');
    if (!page) page = 2;
    else page = +page + 1;
    if (name) return console.log(`/games?name=${name}&page=${page}`);
    setRedirectUrl(`/games?page=${page}`);
  };

  const handleOnChange = (e: any) => {
    setFilterName(e?.target?.value);
  };

  if (redirectUrl) return <Redirect to={redirectUrl} />;

  if (loading) return <Spinner />;
  return (
    <>
      <Header title="List of games" />
      <div className="games-list-container">
        <FilterForm
          handleSubmit={handleOnSubmit}
          handleOnChange={handleOnChange}
          value={filterName}
        />
        <div className="games-list">
          {gamesList.map((gameItem: any) => (
            <Link to={`games/${gameItem.id}`} key={gameItem.id}>
              <GameListItem gameDetails={gameItem} />
            </Link>
          ))}
        </div>
        {showLoadMoreBtn ? (
          <div className="games-list-item" onClick={changePage}>
            <div className="list-item-bg">
              <div className="tint-color"></div>
              <div className="secondary-color"></div>
            </div>
            <button>load more</button>
          </div>
        ) : null}
      </div>
    </>
  );
};

export default GamesList;
