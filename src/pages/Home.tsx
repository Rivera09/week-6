import React, { useContext, useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

import { GameCard, Header, Spinner } from '../components';
import { FEATURED_GAMES } from '../consts';
import { RouterContext } from '../routerContext';

const Home: React.FC = () => {
  const { apiConnection } = useContext<any>(RouterContext);
  const [weekGame, setWeekGame] = useState<any>({});
  const [gamesList, setGamesList] = useState([]);
  const [loading, setLoading] = useState<boolean>(true);
  useEffect(() => {
    const fetchFeaturedGames = async () => {
      const { resBody, error } = await apiConnection.getData(FEATURED_GAMES);
      if (error) return;

      setWeekGame(resBody.splice(0, 1).pop());
      setGamesList(resBody);
      setLoading(false);
    };
    fetchFeaturedGames();
  }, [apiConnection]);

  if (loading) return <Spinner />;
  return (
    <>
      <Header />
      <div className="featured-games">
        <h3>This week&apos;s game </h3>
        <div className="week-game">
          <Link to={`/games/${weekGame?.id}`}>
            <GameCard gameData={weekGame} />
          </Link>
        </div>
        <h3>Featured games</h3>
        <div className="games-carousel">
          {gamesList.map((gameItem: any) => (
            <Link to={`/games/${gameItem.id}`} key={gameItem?.id}>
              <GameCard gameData={gameItem} />
            </Link>
          ))}
        </div>
      </div>
    </>
  );
};

export default Home;
